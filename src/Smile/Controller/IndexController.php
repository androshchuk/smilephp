<?php
namespace Smile\Controller;

use Smarty;
use Smile\Model\FileModel;
use Smile\Model\HomeModel;

/**
 * Class IndexController
 * @package Smile\Controller
 */
class IndexController
{
    /**
     * @var Smarty
     */
    private $smarty;

    /**
     * @var HomeModel
     */
    private $model;

    /**
     * @var FileModel
     */
    private $file;

    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        $this->smarty = new Smarty();
        $this->model = new HomeModel();
        $this->file = new FileModel();
    }

    /**
     * @return mixed
     */
    public function index() {
        if (isset($_FILES['csv'])) {
            try {
                $this->file->processingCsv($_FILES['csv']);
            } catch (\RuntimeException $e) {
                $this->smarty->assign('error', $e->getMessage());
            }
        }
        $productsInStore = $this->model->getProductsInStore();

        $this->smarty->assign('productsInStore', $productsInStore);

        return $this->smarty->display('src/Smile/View/home.tpl');
    }
}