<?php
namespace Smile\Helper;

/**
 * Class ProcessingCsvHelper
 * @package Smile\Helper
 */
class ProcessingCsvHelper
{
    /**
     * Maximum file size in kilobytes
     *
     * @var int
     */
    private $maxUploadSize = 2048;

    /**
     * Allowed file types
     *
     * @var array
     */
    private $allowedUploadType = [
        'text/csv'
    ];

    /**
     * @var array
     */
    protected $file;

    /**
     * ProcessingCsvHelper constructor.
     * @param array $file
     */
    public function __construct(array $file)
    {
        $this->file = $file;
    }

    /**
     * @throws \RuntimeException
     */
    public function validation (): void
    {
        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
            !isset($this->file['error']) ||
            \is_array($this->file['error'])
        ) {
            throw new \RuntimeException('Invalid parameters.');
        }

        // Check $this->file['error'] value.
        switch ($this->file['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new \RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new \RuntimeException('Exceeded file size limit.');
            default:
                throw new \RuntimeException('Unknown errors.');
        }

        // You should also check file size here.
        if ($this->file['size'] > $this->maxUploadSize) {
            throw new \RuntimeException('Exceeded file size limit. Admissible file size ' . $this->maxUploadSize . ' kilobytes.');
        }

        if ($this->file['size'] <= 0) {
            throw new \RuntimeException('File is empty.');
        }

        // Check file type
        if (false === \in_array($this->file['type'], $this->allowedUploadType, true)) {
            throw new \RuntimeException('Invalid file format.');
        }
    }

    /**
     * @throws \RuntimeException
     * @return string
     */
    public function upload(): string
    {
        if (!move_uploaded_file(
            $this->file['tmp_name'],
            $fileName = sprintf('./uploads/%s.csv',
                date('Y-m-d\TH:i:s')
            )
        )) {
            throw new \RuntimeException('Failed to move uploaded file.');
        }

        return $fileName;
    }
}