<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SmilePhp</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>

{if $productsInStore}
    <h1>Залишки на складах:</h1>
    <table border="2">
        <thead>
        <tr>
            <td>Product name</td>
            <td>Qty</td>
            <td>Warehouses</td>
        </tr>
        </thead>
        <tbody>
        {foreach from=$productsInStore item=productInStore}
            <tr>
                <td>{$productInStore['product_name']}</td>
                <td>{$productInStore['quantity']}</td>
                <td>{$productInStore['warehouses']}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{else}
    <h1>На жодному складі немає продуктів :(</h1>
    <h2>Завантажте, будь ласка, csv файл</h2>
{/if}
<br>
<br>
<br>
<form action="/" method="post" enctype="multipart/form-data">
    <input type="file" name="csv">
    <input type="submit">
</form>
<br>
{if !empty($error)}
    <div class="error">
        {$error}
    </div>
{/if}
</body>
</html>