<?php
namespace Smile\Model;

use Smile\Helper\DBConnectionHelper;

class BaseModel
{
    /**
     * @var DBConnectionHelper
     */
    protected $connection;

    /**
     * @var array
     */
    protected $config;

    /**
     * DataBaseTrait constructor.
     */
    public function __construct()
    {
        $this->config = parse_ini_file('config/config.ini');
        try {
            $this->connection = DBConnectionHelper::connection($this->config);
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}
