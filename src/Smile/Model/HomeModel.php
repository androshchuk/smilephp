<?php
namespace Smile\Model;

/**
 * Class HomeModel
 * @package Smile\Model
 */
class HomeModel extends BaseModel
{
    /**
     * @return array
     */
    public function getProductsInStore() : array {
        return $this->connection->query('
            SELECT
            p.product_name AS "product_name",
            SUM(pw.quantity) AS "quantity",
            group_concat(w.warehouse_name ORDER BY w.warehouse_name ASC SEPARATOR \', \') AS "warehouses"
            FROM `products_warehouses` AS pw
            LEFT JOIN products AS p ON p.product_name=pw.`product`
            LEFT JOIN warehouses AS w ON w.warehouse_name=pw.`warehouse`
            WHERE pw.quantity > 0
            GROUP BY p.product_name
            ')
            ->fetchAll(\PDO::FETCH_ASSOC);
    }
}