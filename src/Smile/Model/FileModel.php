<?php
namespace Smile\Model;

use Smile\Helper\ProcessingCsvHelper;

/**
 * Class FileModel
 * @package Smile\Model
 */
class FileModel extends BaseModel
{
    /**
     * @param array $file
     * @throws \RuntimeException
     */
    public function processingCsv(array $file) {
        $csv = new ProcessingCsvHelper($file);
        try {
            $csv->validation();
        } catch (\RuntimeException $e) {
            throw new \RuntimeException('Validation error: ' . $e->getMessage());
        }

        try {
            $this->updateProductBalanceInStock($csv->upload());
        } catch (\RuntimeException $e) {
            throw new \RuntimeException('Error: ' . $e->getMessage());
        }
    }

    /**
     * @param $fileName
     */
    private function updateProductBalanceInStock($fileName) {
        $csv = new \parseCSV($fileName);
        if (!empty($csv->data)) {
            array_walk($csv->data, ['self', 'updateQuantityProductInWarehouse']);
        }
    }

    /**
     * @param array $item
     */
    private function updateQuantityProductInWarehouse(array $item) {
        $sql = 'INSERT INTO products_warehouses (`product`, `warehouse`, `quantity`) VALUES(:productName,:warehouseName, :quantity) ON DUPLICATE KEY UPDATE `product`=:productName2, `warehouse`=:warehouseName2, `quantity`=`quantity`+:quantity2';
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':warehouseName', $item['warehouse'], \PDO::PARAM_STR);
        $stmt->bindValue(':productName', $item['product_name'], \PDO::PARAM_STR);
        $stmt->bindValue(':quantity', $item['qty'], \PDO::PARAM_INT);
        $stmt->bindValue(':warehouseName2', $item['warehouse'], \PDO::PARAM_STR);
        $stmt->bindValue(':productName2', $item['product_name'], \PDO::PARAM_STR);
        $stmt->bindValue(':quantity2', $item['qty'], \PDO::PARAM_INT);
        $stmt->execute();
    }
}