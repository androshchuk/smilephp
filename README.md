Test work for Smile
===================

## Setup project

If you don't have Composer yet, download it following the instructions on http://getcomposer.org/
or just run the following command:

    curl -s http://getcomposer.org/installer | php

Install vendors with

    php composer.phar install
    
Create config file on the base config/config.ini.dist

    cp config/config.ini.dist config/config.ini

Set config for database in file config/config.ini 

For create tables run 

    mysql -uUSER -pPASSWORD SCHEMA < data/sql/init.sql

or import data/sql/init.sql in your schema by using phpmyadmin

## Commands help

Run server with command 

    php -S localhost:8000

CSV test files are in the folder data/csv
