<?php

    use Smile\Controller\IndexController;

    require __DIR__ . '/vendor/autoload.php';

    $index = new IndexController;
    $index->index();
