CREATE TABLE `products` (
  `product_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `products_warehouses` (
  `product` varchar(255) NOT NULL,
  `warehouse` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `warehouses` (
  `warehouse_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `products`
  ADD PRIMARY KEY (`product_name`);

ALTER TABLE `products_warehouses`
  ADD PRIMARY KEY (`product`,`warehouse`),
  ADD KEY `warehouse` (`warehouse`);

ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`warehouse_name`),
  ADD UNIQUE KEY `warehouse_name` (`warehouse_name`);

ALTER TABLE `products_warehouses`
  ADD CONSTRAINT `products_warehouses_ibfk_1` FOREIGN KEY (`product`) REFERENCES `products` (`product_name`),
  ADD CONSTRAINT `products_warehouses_ibfk_2` FOREIGN KEY (`warehouse`) REFERENCES `warehouses` (`warehouse_name`);

DELIMITER $$
CREATE TRIGGER `products_warehouses_before_insert` BEFORE INSERT ON `products_warehouses` FOR EACH ROW BEGIN
        INSERT IGNORE INTO products SET product_name = NEW.product;
        INSERT IGNORE INTO warehouses SET warehouse_name = NEW.warehouse;
END
$$
DELIMITER ;